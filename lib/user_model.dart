class UserModel {
  final int id;
  final String firstname;
  
  UserModel({required this.id, required this.firstname});

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(id: json['id'], firstname: json['first_name']);
  }
}