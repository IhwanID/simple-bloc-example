import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_example/bloc/app_blocs.dart';
import 'package:flutter_bloc_example/bloc/app_events.dart';
import 'package:flutter_bloc_example/bloc/app_states.dart';
import 'package:flutter_bloc_example/repositories.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: RepositoryProvider(create: (context) => UserRepository(),
      child: const Home(),
      )
    );
  }
}

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: ((context) => UserBloc(RepositoryProvider.of<UserRepository>(context),
    )..add(LoadUserEvent())),
    child: Scaffold(appBar: AppBar(title: Text("Bloc App"),),
    body: BlocBuilder<UserBloc, UserState>(builder: (context, state) {
      if (state is UserLoadingState) {
        return Center(child: CircularProgressIndicator());
      }

      if (state is UserLoadedState) {
        return Center(child: Text("${state.users.map((e) => e.firstname)}"),);
      }

      if (state is UserErrorState) {
        return Center(child: Text("${state.error}"),);
      }

      return Container();
    },),),
    );
  }
}
